const multer = require('multer');
const { pathSaveFile } = require('./configs.js');

const storage = multer.diskStorage({
  fileFilter: (req, file, cb) => {
    switch (file.mimetype) {
      case 'image/png':
        cb(null, `image-${Date.now()}.png`);
        break;
      case 'image/jpeg':
        cb(null, `image-${Date.now()}.jpg`);
        break;
      default:
        cb(null, false);
    }
  },
  destination: (req, file, cb) => {
    cb(null, pathSaveFile);
  },
  filename: (req, file, cb) => {
    switch (file.mimetype) {
      case 'image/png':
        cb(null, `image-${Date.now()}.png`);
        break;
      case 'image/jpeg':
        cb(null, `image-${Date.now()}.jpg`);
        break;
      default:
        cb(null, false);
    }
  },
});
const upload = multer({
  storage: storage,
  limits: {
    fileSize: 5 * 1024 * 1024,
  },
  fileFilter: function (req, file, cb) {
    if (file.mimetype !== 'image/png' && file.mimetype !== 'image/jpeg') {
      req.fileValidationError =
        'Не удалось загрузить изображение. Размер файла больше 5 МБ или имеет неправильное расширение!';
      return cb(null, false);
    }
    cb(null, true);
  },
});

module.exports = {
  upload,
};

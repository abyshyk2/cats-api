import Client from '../../dev/http-client';
import type { CatMinInfo, CatsList } from '../../dev/types';
import is from "@sindresorhus/is";
import string = is.string;

const cats: CatMinInfo[] = [{ name: 'Писечкин-сисечкин', description: '', gender: 'male' }];

let catId;

const fakeId = 'fakeId';

const HttpClient = Client.getInstance();

describe('Автоматизация тестов на поиск котика, добавления описания котику и сортировку котиков', () => {
   beforeAll(async () => {
        try {
            const add_cat_response = await HttpClient.post('core/cats/add', {
                responseType: 'json',
                json: { cats },
            });
            if ((add_cat_response.body as CatsList).cats[0].id) {
                catId = (add_cat_response.body as CatsList).cats[0].id;
            } else throw new Error('Не получилось получить id тестового котика!');
        } catch (error) {
            throw new Error('Не удалось создать котика для автотестов!');
        }
    });

   afterAll(async () => {
       await HttpClient.delete(`core/cats/${catId}/remove`, {
          responseType: 'json',
       });
    });

    it('Добавить котику описание', async () => {
        const response = await HttpClient.post(`core/cats/save-description`, {
            responseType: 'json',
            json: {
                catId: catId,
                catDescription: 'Описание тестовое'
            }
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toMatchObject({
            id: catId,
            description: 'Описание тестовое',
        });
    });

    it('Найти существующего котика', async () => {
        const response = await HttpClient.get(`core/cats/get-by-id?id=${catId}`, {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toMatchObject({
            cat: {
                id: catId,
                name: 'Писечкин-сисечкин',
            }
        });
    });

    it('Найти котика c некорректным id', async () => {
        await expect(
            HttpClient.get(`core/cats/get-by-id?id=${fakeId}`, {
                responseType: 'json',
            })
        ).rejects.toThrowError('Response code 400 (Bad Request)');
    });

    it('Список котов сгруппированных по группам', async () => {
        const response = await HttpClient.get('core/cats/allByLetter', {
            responseType: 'json',
        });
        expect(response.statusCode).toEqual(200);

        expect(response.body).toEqual({
            groups: expect.arrayContaining([
                expect.objectContaining({
                    title: expect.any(String),
                    cats: expect.arrayContaining([
                        expect.objectContaining({
                            id: expect.any(Number),
                            name: expect.any(String),
                            description: expect.any(String),
                            gender: expect.any(String),
                            tags: null,
                            likes: expect.any(Number),
                            dislikes: expect.any(Number),
                            count_by_letter: expect.any(String),
                        }),
                    ]),
                    count_in_group: expect.any(Number),
                    count_by_letter: expect.any(Number),
                }),
            ]),
            count_all: expect.any(Number),
            count_output: expect.any(Number),
        });
    });
});

